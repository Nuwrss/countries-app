package com.dgd.countriesdb.viewmodel;

import android.app.Application;


import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.dgd.countriesdb.model.Country;
import com.dgd.countriesdb.repository.database.Database;
import com.dgd.countriesdb.repository.listener.DataBaseResponseListener;
import com.dgd.countriesdb.repository.listener.NetworkResponseListener;
import com.dgd.countriesdb.repository.network.Network;

import com.dgd.countriesdb.utilis.NetworkStatus;


import java.util.List;

public class CountriesViewModel extends AndroidViewModel {
    private MutableLiveData<List<Country>> countries ;
    private MutableLiveData<NetworkStatus> netWorkLoadingStatus  = new MutableLiveData<>();
    public CountriesViewModel(@NonNull Application application) {
        super(application);
        netWorkLoadingStatus.postValue(NetworkStatus.NOT_STARTED);
    }

    public MutableLiveData<List<Country>> getCountries(boolean retry) {

        if (!retry){
            if (countries!=null)return countries;
            countries =new MutableLiveData<>();
        }


        netWorkLoadingStatus.postValue(NetworkStatus.LOADING);

        Network.getInstance(getApplication()).getCountries(new NetworkResponseListener() {
            @Override
            public void onCountriesLoaded(List<Country> countriesList) {
                saveCountriesToDataBase(countriesList);
                netWorkLoadingStatus.postValue(NetworkStatus.SUCCESS);
                countries.postValue(countriesList);
            }

            @Override
            public void onFailedToLoad() {
                netWorkLoadingStatus.postValue(NetworkStatus.FAILED);
                loadingCountriesFromDatabase(null);
            }
        });
        return countries;
    }

    public MutableLiveData<List<Country>> getCountries(final String[] codes, boolean retry) {
        if (!retry){
            if (countries!=null)return countries;
            countries =new MutableLiveData<>();
        }
        netWorkLoadingStatus.postValue(NetworkStatus.LOADING);
        String countriesCodes="";
        for (int i=0; i<codes.length;i++){
            if (i==codes.length-1){
                countriesCodes=countriesCodes+codes[i];
            }else
            countriesCodes=countriesCodes+codes[i]+";";
        }

        Network.getInstance(getApplication()).getCountries(countriesCodes,new NetworkResponseListener() {
            @Override
            public void onCountriesLoaded(List<Country> countriesList) {
                saveCountriesToDataBase(countriesList);
                netWorkLoadingStatus.postValue(NetworkStatus.SUCCESS);
                countries.postValue(countriesList);
            }

            @Override
            public void onFailedToLoad() {
                netWorkLoadingStatus.postValue(NetworkStatus.FAILED);
                loadingCountriesFromDatabase(codes);
            }
        });
        return countries;
    }

    private void saveCountriesToDataBase(List<Country> countriesList) {

        Database.getInstance(getApplication()).insertCountries( countriesList);
    }

    private void loadingCountriesFromDatabase(String[] codes) {
        if (codes==null) {
            Database.getInstance(getApplication()).getCountries(new DataBaseResponseListener() {
                @Override
                public void onCountriesLoaded(List<Country> countriesList) {
                    countries.postValue(countriesList);
                }

                @Override
                public void onCountryLoaded(Country country) {

                }

                @Override
                public void onFailedToLoad() {

                }
            });
        }else{
            Database.getInstance(getApplication()).getCountriesByCodes( codes, new DataBaseResponseListener() {
                @Override
                public void onCountriesLoaded(List<Country> countriesList) {
                    countries.postValue(countriesList);
                }

                @Override
                public void onCountryLoaded(Country country) {

                }

                @Override
                public void onFailedToLoad() {

                }
            });
        }
    }

    public MutableLiveData<NetworkStatus> getNetWorkLoadingStatus(){

        return netWorkLoadingStatus;

    }


}
