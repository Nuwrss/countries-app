package com.dgd.countriesdb;



import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import com.dgd.countriesdb.model.Country;
import java.util.List;

public class CountryBordersActivity extends MainActivity {

    private Country country;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Override
    public void setToolbar(){
        super.setToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        country = (Country) getIntent().getSerializableExtra("country");
        setTitle(country.getName()+ " Borders");
    }
    @Override
    public void setCountriesViewModel(){

        if (country.getBorders()==null || country.getBorders().length==0){
            hideProgress();
            showCountryWithoutBordersCountries();
        }else {
            countriesViewModel.getCountries(country.getBorders(),false).observe(this, new Observer<List<Country>>() {
                @Override
                public void onChanged(List<Country> countries) {


                   sort(countries,sortBy, sortType);

                }
            });
        }
    }
    @Override
    public void tryLoadingFromInternetAgain() {
        countriesViewModel.getCountries(country.getBorders(),true);
    }
    private void showCountryWithoutBordersCountries() {
        AlertDialog alertDialog = new AlertDialog.Builder(CountryBordersActivity.this).create();
        alertDialog.setTitle("Countries Db");
        alertDialog.setMessage(country.getName()+" doesn't have any borders with any other countries");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }
}
