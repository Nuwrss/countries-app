package com.dgd.countriesdb.repository.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dgd.countriesdb.model.Country;
import com.dgd.countriesdb.utilis.Constants;

@Database(entities = {Country.class }, version = 1, exportSchema = false)
public abstract class CountriesDataBase extends RoomDatabase {
    public abstract CountryDao countryDao();

    private static  CountriesDataBase INSTANCE;
    public static CountriesDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {


            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    CountriesDataBase.class, Constants.COUNTRIES_DB_NAME)
                    .build();


        }
        return INSTANCE;
    }
}
