package com.dgd.countriesdb.repository.database;

import android.app.Application;
import android.os.AsyncTask;

import com.dgd.countriesdb.model.Country;
import com.dgd.countriesdb.repository.listener.DataBaseResponseListener;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private Application application;
    private static Database database;

    private Database(Application application) {
        this.application=application;
    }

    public static Database getInstance(Application application) {
        if (database==null){
            database=new Database(application);
        }
        return database;
    }



    public  void getCountries(final DataBaseResponseListener dataBaseResponseListener) {
        new AsyncTask<Void, Void, List<Country>>() {
            @Override
            protected void onPostExecute(List<Country> countries) {
                super.onPostExecute(countries);
                dataBaseResponseListener.onCountriesLoaded(countries);
            }

            @Override
            protected List<Country> doInBackground(Void... voids) {

                return CountriesDataBase.getDatabase(application).countryDao().getAllCountries();

            }


        }.execute();
    }

    public void updateCountries(final List<Country>countries){
        new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... voids) {
                if (countries!=null ) {

                     CountriesDataBase.getDatabase(application).countryDao().updateCountries(countries);
                }

                return null;
            }


        }.execute();

    }
    public void insertCountries(final List<Country>countries){
        new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... voids) {
                if (countries!=null ) {

                    CountriesDataBase.getDatabase(application).countryDao().insertCountries(countries);
                }

                return null;
            }


        }.execute();

    }


    private void getCountry(final String code , final DataBaseResponseListener dataBaseResponseListener){
        new AsyncTask<Void, Void, Country>() {
            @Override
            protected void onPostExecute(Country country) {
                super.onPostExecute(country);
                dataBaseResponseListener.onCountryLoaded(country);
            }

            @Override
            protected Country doInBackground(Void... voids) {

                return CountriesDataBase.getDatabase(application).countryDao().getCountry(code);

            }


        }.execute();
    }

    public void getCountriesByCodes(final String[] codes, final DataBaseResponseListener dataBaseResponseListener) {
        final ArrayList<Country> countries = new ArrayList<>();
        for (String code : codes){
            getCountry(code, new DataBaseResponseListener() {
                @Override
                public void onCountriesLoaded(List<Country> countries) {

                }

                @Override
                public void onCountryLoaded(Country country) {
                    countries.add(country);
                    if (codes.length==countries.size()){
                        dataBaseResponseListener.onCountriesLoaded(countries);
                    }
                }

                @Override
                public void onFailedToLoad() {

                }
            });
        }

    }
}
