package com.dgd.countriesdb.repository.network;

import android.content.Context;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dgd.countriesdb.model.Country;
import com.dgd.countriesdb.repository.listener.NetworkResponseListener;
import com.dgd.countriesdb.utilis.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class Network {
    private static RequestQueue queue;
    private static Network network;

    public static Network getInstance(Context context) {
        if (network==null){

            queue = Volley.newRequestQueue(context);
            network=new Network();
        }
        return network;
    }
    private Network(){

    }

    public void getCountries( final NetworkResponseListener networkResponseListener){



        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.COUNTRIES_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Gson gson = new Gson();

                        Type listCountries = new TypeToken<List<Country>>(){}.getType();
                        List<Country> countries = gson.fromJson(response, listCountries);
                        networkResponseListener.onCountriesLoaded(countries);






                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                networkResponseListener.onFailedToLoad();

            }
        });
        // to check if data saved to database
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public void getCountries(String codes,final NetworkResponseListener networkResponseListener){



        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.COUNTRIES_CODE_URL+codes,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Gson gson = new Gson();

                        Type listCountries = new TypeToken<List<Country>>(){}.getType();
                        List<Country> countries = gson.fromJson(response, listCountries);
                        networkResponseListener.onCountriesLoaded(countries);






                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                networkResponseListener.onFailedToLoad();

            }
        });
        // to check if data saved to database
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public void stopLoading(){

       queue.stop();
    }

}
