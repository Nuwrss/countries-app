package com.dgd.countriesdb.repository.listener;

import com.dgd.countriesdb.model.Country;

import java.util.List;

public interface NetworkResponseListener {
    void onCountriesLoaded(List<Country> countries);
    void onFailedToLoad();
}
