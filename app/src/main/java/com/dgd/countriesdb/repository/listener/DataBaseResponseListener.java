package com.dgd.countriesdb.repository.listener;

import com.dgd.countriesdb.model.Country;

import java.util.List;

public interface DataBaseResponseListener {
    void onCountriesLoaded(List<Country> countries);
    void onCountryLoaded(Country country);
    void onFailedToLoad();
}
