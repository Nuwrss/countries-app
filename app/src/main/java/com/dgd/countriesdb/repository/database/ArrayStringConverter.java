package com.dgd.countriesdb.repository.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class ArrayStringConverter {
    @TypeConverter
    public static String[] fromString(String value) {
        Type listType = new TypeToken<String[]>() {}.getType();
        return new Gson().fromJson(value, listType);
    }
    @TypeConverter
    public static String fromArray(String[] values) {
        Gson gson = new Gson();
        String json = gson.toJson(values);
        return json;
    }
}
