package com.dgd.countriesdb.repository.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dgd.countriesdb.model.Country;


import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CountryDao {

    @Insert(onConflict = REPLACE)
    void insertCountries(List<Country> countries);

    @Query("DELETE FROM countries")
    void deleteAll();

    @Update
    void updateCountries(List<Country> countries);

    @Query("SELECT * from countries")
    List<Country> getAllCountries();

    @Query("SELECT * from countries WHERE alpha3Code LIKE :code ")
    Country getCountry(String code);
}
