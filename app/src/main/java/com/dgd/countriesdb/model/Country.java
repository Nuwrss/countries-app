package com.dgd.countriesdb.model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.dgd.countriesdb.repository.database.ArrayStringConverter;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.Comparator;

import java.util.List;
import java.util.Objects;

@Entity(tableName = "countries")
public class Country implements Serializable {
    @NonNull
    @PrimaryKey
    private String name;
    private double area;
    @TypeConverters(ArrayStringConverter.class)
    private String[] borders;
    private String nativeName;
    private String alpha3Code;

    public String getAlpha3Code() {
        return alpha3Code;
    }

    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public String[] getBorders() {
        return borders;
    }

    public void setBorders(String[] borders) {
        this.borders = borders;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Double.compare(country.area, area) == 0 &&
                name.equals(country.name) &&
                Arrays.equals(borders, country.borders) &&
                nativeName.equals(country.nativeName);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, area, nativeName);
        result = 31 * result + Arrays.hashCode(borders);
        return result;
    }

    public static class NameSorter implements Comparator<Country>{

     @Override
     public int compare(Country o1, Country o2) {
        return o1.name.compareTo(o2.name);
     }

    }

    public static class AreaSorter implements Comparator<Country>{

        @Override
        public int compare(Country o1, Country o2) {
           return Double.compare(o1.area, o2.area);

        }

    }

    public static class CountriesDiffUtil extends DiffUtil.Callback {
        List<Country> oldCountries;
        List<Country> newCountries;

        public CountriesDiffUtil(ArrayList<Country> oldCountries, List<Country> newCountries) {
            this.oldCountries=oldCountries;
            this.newCountries=newCountries;
        }

        @Override
        public int getOldListSize() {
            return oldCountries.size();
        }

        @Override
        public int getNewListSize() {
            return newCountries.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldCountries.get(oldItemPosition).getName() .equals( newCountries.get(newItemPosition).getName());

        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldCountries.get(oldItemPosition).equals(newCountries.get(newItemPosition));

        }
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {

            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }

}
