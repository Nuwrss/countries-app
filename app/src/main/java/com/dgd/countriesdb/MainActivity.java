package com.dgd.countriesdb;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;


import com.dgd.countriesdb.adapters.CountriesAdapter;
import com.dgd.countriesdb.model.Country;
import com.dgd.countriesdb.utilis.NetworkStatus;
import com.dgd.countriesdb.viewmodel.CountriesViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public CountriesViewModel countriesViewModel;
    public CountriesAdapter countriesAdapter;
    private  RecyclerView recyclerview;
    private ProgressBar progressBar;
   public CountriesAdapter.SortBy sortBy = null ;
    public CountriesAdapter.SortType sortType = CountriesAdapter.SortType.DEFAULT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            sortType = (CountriesAdapter.SortType) savedInstanceState.getSerializable("sortType");
            sortBy = (CountriesAdapter.SortBy) savedInstanceState.getSerializable("sortBy");
        }
         setToolbar();

        setViews();

        setCountriesViewModelNetwork();
        setCountriesViewModel();

    }

    public void setToolbar(){
        Toolbar myToolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
    }

    private void setViews(){

        progressBar =findViewById(R.id.progress);
        recyclerview = findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayoutManager);




        recyclerview.setItemAnimator(new DefaultItemAnimator());
        countriesAdapter=new CountriesAdapter(this);
        countriesAdapter.setOnCountryClickListener(new CountriesAdapter.OnCountryClickListener() {
            @Override
            public void onCountryClicked(Country country) {

                Intent intent = new Intent(MainActivity.this,CountryBordersActivity.class);
                intent.putExtra("country",country);
                startActivity(intent);

            }
        });
        recyclerview.setAdapter(countriesAdapter);



    }

    private void setCountriesViewModelNetwork(){
        countriesViewModel= ViewModelProviders.of(this).get(CountriesViewModel.class);
        countriesViewModel.getNetWorkLoadingStatus().observe(this, new Observer<NetworkStatus>() {
            @Override
            public void onChanged(NetworkStatus networkStatus) {
                switch(networkStatus) {
                    case SUCCESS:
                        hideProgress();
                        break;

                    case LOADING:
                        showProgress();
                        break;

                    case FAILED:
                        hideProgress();
                        showErrorDialog();
                        break;

                }


            }
        });


    }

    public void setCountriesViewModel(){
        countriesViewModel.getCountries(false).observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countries) {

                sort(countries,sortBy, sortType);

            }
        });
    }
    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);

    }
    public void hideProgress(){
        progressBar.setVisibility(View.GONE);

    }
    private void showErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Countries Db");
        alertDialog.setMessage("Problem with updating countries from internet , do you want to try again ?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        tryLoadingFromInternetAgain();
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void tryLoadingFromInternetAgain() {
        countriesViewModel.getCountries(true);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.sortnameascending:
                sortBy= CountriesAdapter.SortBy.NAME;
                sortType = CountriesAdapter.SortType.ASC;
                break;
            case R.id.sortnamedescending:
                sortBy= CountriesAdapter.SortBy.NAME;
                sortType = CountriesAdapter.SortType.DES;
                break;
            case R.id.sortareaascending:
                sortBy= CountriesAdapter.SortBy.AREA;
                sortType = CountriesAdapter.SortType.ASC;

                break;
            case R.id.sortareaddescending:
                sortBy= CountriesAdapter.SortBy.AREA;
                sortType = CountriesAdapter.SortType.DES;

               break;
            default:
                return super.onContextItemSelected(item);
        }

        sort(null,sortBy, sortType);
        return true;
    }

    public void sort(List<Country>countries ,CountriesAdapter.SortBy sortBy , CountriesAdapter.SortType sortType){

        countriesAdapter.addCountries(countries,sortBy,sortType);
        recyclerview.postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerview.scrollToPosition(0);
            }
        },100);

    }
    @Override
     protected void onSaveInstanceState(Bundle outState) {
           super.onSaveInstanceState(outState);

            outState.putSerializable("sortType", sortType);
            outState.putSerializable("sortBy", sortBy);


         }




}
