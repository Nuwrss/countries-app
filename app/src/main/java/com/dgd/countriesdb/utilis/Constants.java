package com.dgd.countriesdb.utilis;

public class Constants {
    public static final String COUNTRIES_URL = "https://restcountries.eu/rest/v2/all?fields=name;area;nativeName;borders;alpha3Code";
    public static final String COUNTRIES_CODE_URL ="https://restcountries.eu/rest/v2/alpha?codes=";
    public static final String COUNTRIES_DB_NAME = "countries_db" ;
}
