package com.dgd.countriesdb.adapters;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;




import com.dgd.countriesdb.R;
import com.dgd.countriesdb.model.Country;


import java.util.ArrayList;
import java.util.Collections;

import java.util.List;


public class CountriesAdapter extends  RecyclerView.Adapter<CountriesAdapter.CountryViewHolder> {

    private Context context;

    private ArrayList<Country> countries=new ArrayList<>();


    public void setOnCountryClickListener(OnCountryClickListener onCountryClickListener) {
        this.onCountryClickListener = onCountryClickListener;
    }

    private OnCountryClickListener onCountryClickListener;

    public CountriesAdapter( Context context) {


        this.context=context;
    }

    @NonNull
    @Override
    public CountriesAdapter.CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_cell, parent, false);

        return new CountriesAdapter.CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CountriesAdapter.CountryViewHolder holder, int position) {
        final Country country = countries.get(position);

        holder.name.setText(country.getName());
        holder.nativeName.setText(country.getNativeName());






        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCountryClickListener.onCountryClicked(country);
            }
        });

    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public  class CountryViewHolder extends RecyclerView.ViewHolder {




        public TextView nativeName;

        public TextView name;



        public CountryViewHolder(View view) {
            super(view);


            nativeName=view.findViewById(R.id.nativeName);
            name=view.findViewById(R.id.name);

        }
    }
    private void addCountriesToList(List<Country> countryList) {
        final Country.CountriesDiffUtil diffCallback = new Country.CountriesDiffUtil(this.countries, countryList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);


        diffResult.dispatchUpdatesTo(this);
        this.countries.clear();
        this.countries.addAll(countryList);




    }

    public void  addCountries(List<Country> countryLis,SortBy sortBy ,SortType sortType){
        if (countryLis==null){
            countryLis=new ArrayList<>(countries);
        }
        if (sortType!=SortType.DEFAULT){
            if (sortBy==SortBy.NAME){

                Collections.sort(countryLis, new Country.NameSorter());
            }else{
                Collections.sort(countryLis, new Country.AreaSorter() );
            }
            if (sortType==SortType.DES)
                Collections.reverse(countryLis);
        }


        this.addCountriesToList(countryLis);
    }



    public enum SortBy{
       NAME,AREA
    }
    public enum SortType{
        DEFAULT, ASC,DES
    }

    public interface OnCountryClickListener{
        void onCountryClicked(Country country);
    }
}
